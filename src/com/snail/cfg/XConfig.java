/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.snail.cfg;

import com.woniu.utils.io.file.TextReader;
import java.io.File;
import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author pm
 */
public class XConfig {

    private static Element xml;

    static {
        try {
            init();
        } catch (IOException ex) {
            XLogger.logger.error("init error : ", ex);
        }
    }

    public static void init() throws IOException {
        //String file = Jar.getPath(XConfig.class) + "/config/client.xml";
        String file = "config/client.xml";
        Document doc = Jsoup.parseBodyFragment(new TextReader("UTF-8").setSource(file).getContents());
        doc.outputSettings().prettyPrint(false); 
        xml = doc;
    }

    public static Element cfg() {
        return xml.select("body>client").first();
    }

    public static String getTimezone() {
        return cfg().attr("timezone");
    }
}
